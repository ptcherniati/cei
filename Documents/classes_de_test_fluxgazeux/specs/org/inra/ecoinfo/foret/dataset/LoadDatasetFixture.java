package org.inra.ecoinfo.foret.dataset;

import org.inra.ecoinfo.ConcordionSpringJunit4ClassRunner;
import org.inra.ecoinfo.foret.ForetTransactionalTestFixtureExecutionListener;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ptcherniati
 */
@RunWith(ConcordionSpringJunit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class)
@TestExecutionListeners(listeners = {ForetTransactionalTestFixtureExecutionListener.class})
public class LoadDatasetFixture extends org.inra.ecoinfo.dataset.LoadDatasetFixture {

    /**
     *
     */
    public LoadDatasetFixture() {
        super();
        MockitoAnnotations.initMocks(this);
    }
}
