---
title: Campagne CEI 2023
subtitle: Rapport d'activité de fin de carrière
author:
  - TCHERNIATINSKY Philippe
date: 04/04/2023
lang: fr
numbersections: true
documentclass: scrreprt
toc: true
toc-depth: 6
toc-title: Table des matières
fontsize: 12pt
linestretch: 1
linkcolor: black


---

# Rapport d'activité de fin de carrière

En tant que développeur d'application, mon travail consiste à concevoir, créer et maintenir des applications
à destination de la communauté scientifique, pour proposer un système de gestion de données environnementales.
Cela implique de travailler en équipe avec des gestionnaires de projet (correspondants) pour s'assurer que l'application
fonctionne et réponde au besoin de l'utilisateur.

Pour arriver à ce but, je peux mettre en œuvre une solide compréhension des principes de programmation[^1]. Un esprit
logique [^2]:
qui me permet de poser efficacement les problématiques afin d'y apporter des solutions. Je peux aussi m'appuyer sur mon
esprit
d'analyse pour comprendre les besoins, évaluer les possibilités et les risques [^3]. Une curiosité qui me permet de
comprendre
et de m'approprier les évolutions technologiques et méthodologiques [^4].

Mais pour ce rapport de carrière, je voudrais apporter un éclairage sur ma contribution "créative" au code.

Pour cela, je commencerai par apporter une définition de la créativité en particulier dans le développement
informatique.
Je montrerai ensuite comment j'ai utilisé ma créativité pour améliorer le processus de développement de mon équipe en 
m'appuyant sur des exemples concrets de solutions créatives que j'ai implémentées ou mise en place.

## Mais qu'est-ce que la créativité et surtout dans un domaine aussi logique que l'informatique ?

La créativité, c'est la capacité, le pouvoir, qu'a un individu de créer, c'est-à-dire d'imaginer et de réaliser
quelque chose de nouveau. C'est une compétence qui est aujourd'hui assez périphérique dans le cursus scolaire.
On pense avant tout à nos cours d'arts plastiques ou de musiques dispensés dans les combles de l'école. La créativité
fait aussi penser au beau. Je peux être tout autant émerveillé par un tableau impressionniste ou une symphonie qu'à une
solution originale, fournie par les mathématiques. Par exemple, en informatique, un arbre binaire permet d'accéder à un
mot d'un dictionnaire en seulement 15 itérations [^5]. C'est une forme de beau ! On parlera aussi d'originalité,
d'innovation, d'ingéniosité, de réflexion, d'ouverture d'esprit, de pensée divergente, d'inventivité, de curiosité,
de flexibilité, de pensée latérale, d'imagination, d'intuition, de flexibilité... Déjà, on entrevoit ce que la
créativité
peut apporter au développement.

On peut être créatif dans les méthodes de travail. Que ce soit dans des objectifs d'efficacité, de qualité, de
productivité,
de collaboration, d'innovation, de respect des normes ou de sécurité. Si des solutions éprouvées existent, la créativité
permet d'adapter ces solutions à la réalité de l'équipe, des projets ou de l'unité. Je me suis attaché par exemple à
créer
des moments d'échange dans la cellule ou l'unité pour que les idées puissent être débattues en confrontant des points de
vue.
Je reviendrai plus tard sur la genèse du noyau de l'application qui en est un bon exemple. Je pense ici par exemple à
deux
présentations que j'ai faites en assemblée générale. L'une pour présenter le web sémantique, l'autre pour présenter
l'application OpenADOM. Le soin apporté à ces présentations et la créativité dont j'ai fait preuve, ont permis, je
pense, à
leur appropriation par l'unité [^6].

La créativité permet de questionner "l'évidence". Quand on change de domaine cette évidence s'estompe. Lorsque l'on
modélise,
une notion aussi évidente que la variable pose des problèmes quand on essaie de faire cohabiter des données forestières,
lacustres,
de conduite culturale, de produits résiduels organiques... Plus l'on recherche la généricité, plus il faut questionner
ces
fondamentaux. Il devient nécessaire d'avoir des data scientists à la frontière entre la recherche et l'informatique. Je
me suis moi-même
posé la question [^7] de la définition de la variable dans le cadre des ORE.

Pour l'écriture du code, on doit faire preuve de créativité pour débloquer une situation. Pour cela, on peut s'appuyer
sur différentes méthodes :

- Le hacking permet de détourner des outils ou des fonctionnalités existantes pour créer quelque chose de nouveau.
  L'avantage est de faire
  du vieux avec du neuf. Cela est économique pour arriver à un résultat. <br/>Le risque est de créer des failles dans le
  code.
- Le refactoring est un autre exemple. Il est utilisé afin de limiter la répétition de code. Il permet aussi d'abstraire
  les concepts de deux notions au départ différentes pour trouver de la généricité et ouvrir de nouvelles perspectives
  dans un code
  maintenable et évolutif. <br/>Pour limiter le risque la pratique des tests permet au développeur d'être rassuré quant à la
  finalité
  de la réécriture du code.
- Le changement de paradigme, peut être la solution quand on arrive au bout des solutions viables. Tout remettre sur le
  tapis,
  repenser le besoin, en y intégrant les nouvelles techniques et technologies, et se donner un nouveau souffle et de
  nouvelles perspectives.
  La créativité est là essentielle pour être innovant et moderne. Il faut accompagner ce changement, accepter de perdre
  des fonctionnalités,
  au profit de nouvelles perspectives et rassurer les utilisateurs dans cette perte de repères.
- S'appuyer sur ce qui se fait, sur les bonnes pratiques, sur les standards ou des patrons de conception, permet aussi
  d'être plus créatif. En se libérant de charges complexes (architecture, sécurité ...), le développeur peut se
  concentrer
  sur la partie métier de son travail et proposer des solutions originales et efficaces. Il faut cependant veiller à ce
  que ce cadre
  ne soit pas un frein aux objectifs à atteindre.
- Utiliser de nouveaux outils, de nouvelles méthodes, de nouvelles technologies permettent de révéler des problématiques
  cachées
  et d'amener le développeur à repenser son développement et sa pratique. Même si souvent, la veille technologique
  n'amène pas de changements radicaux immédiats,
  elle permet d'introduire ces changements progressivement pour un code sans cesse de meilleure qualité.

On le voit, la créativité peut intervenir tout au long du développement et prendre une place importante dans mon travail
et mes relations avec mes collègues. Je vais maintenant l'illustrer au travers d'exemples concrets.

## La créativité au quotidien dans mon travail

En [communication relationnelle,](https://www.institut-repere.com/info/process-communication-6-types-personnalite/) je
suis
défini comme un "rêveur rebelle". Mon domaine de prédilection est "les idées". Dans mon travail, j'estime faire un
travail
de recherche (veille technologique). Je me documente sur les nouveautés et il n'est pas rare que j'essaie de nouvelles
approches,
de nouvelles technologies ou simplement que je partage avec mes collègues sur leur façon de faire. Même si parfois mes
essais
n'aboutissent à rien, si une partie de mon code fini à la corbeille, je n'estime jamais que c'est une perte de temps.
Dans certains cas parce que c'est trop tôt. D'autres fois, j'en tire la conclusion que cela n'est pas faisable et souvent cela me
permet de trouver la manière de faire. À tous les coups cela enrichit mon expérience.

J'aime partager ces pratiques avec
mes collègues et suis fréquemment sollicité pour débloquer une situation. Dans mes fonctions d'encadrement, j'encourage
les personnes que j'accompagne à "essayer" et je valorise leur travail même s'il n'aboutit pas à un résultat. On
s'enrichit
de ses erreurs et ne pas en faire n'est pas un gage de qualité.

Dans mes rapports annuels, je mets en avant ces idées. J'ai toujours été remarqué par ma hiérarchie sur ce point, et je
la
remercie de m'avoir laissé le champ libre pour leur mise en œuvre.

> <h4>Entretien 2022 directeur d'unité:</h4>
>
> ...
>
> _Finalement, je souhaite également le remercier pour son investissement dans les réflexions qui ont conduit à
> l’établissement
> de la nouvelle unité : il a apporté sa vision de la place que devait prendre la partie “data”._


Je reconnais que lorsque j'expose un panel de possibilités,
je peux embrouiller mes interlocuteurs et devenir incompréhensible. Mais je sais aussi défendre les idées que je juge
pertinentes ou celles qui concernent un risque majeur et j'essaie toujours de rendre le service demandé en gardant un
esprit
d'ouverture.

### Communication

Comment travaille-t-on ensemble au sein d'un collectif ? Comment peut-on co-construire une application, des projets ?

Cette question, je me la pose souvent et je cherche à chaque fois des "idées" pour que l'information circule. Même
lorsque
l'on travaille sur des tâches distinctes, il est toujours intéressant de confronter ses difficultés, ses célébrations
et ses pratiques. La question du collectif a aussi été mise à mal à la suite de la crise sanitaire, puis du
développement
du télétravail.

Dans la mise en place de la nouvelle unité (info&sols), je suis un défenseur du temps qui ne se voit pas.
Du temps pour s'arrêter sur ce que l'on a fait pour en tirer le positif et le négatif ; du temps pour réfléchir à ce que
l'on
pourrait faire (par exemple : pour créer des outils qui nous permettront de répondre plus efficacement aux demandes
futures) ;
du temps pour se former, essayer, se tromper ; du temps pour découvrir de nouvelles façons de faire dans les échanges de
pratiques...

J'apprécie par exemple les séminaires et formations (CATI, PEPI) ces moments d'échanges qui "reboost" et nous redonnent
le sentiment d'appartenir à un collectif. Ces échanges sont rarement immédiatement "productifs", mais ils influent sur
notre travail à plus long terme. On pourrait aussi placer ici les échanges informels de la cafétéria ou du restaurant ou
l'assemblée générale itinérante pour laquelle j'ai pu, par trois fois, m'associer à sa
préparation.
Ces moments permettent plus tard d'avoir des rapports facilités avec ses interlocuteurs.

Je vais particulièrement décrire deux moments de ma carrière où la créativité prenait sa place dans la communication.

Le premier exemple est lors de la fusion des projets GLACPE et FORET, qui avaient divergés, et qui a abouti à la
création d'un "noyau" pour
l'application des ORE.

Avant que je sois en poste à Infosol, j'ai travaillé à EEF (Ecologie et Ecosystèmes forestiers) à Nancy en collaboration
avec le chef de
projet Antoine SCHELLENBERGER et l'animateur (et encadrant) Damien MAURICE. Je travaillais à une application
d'intégration
des données forestières en m'appuyant sur les développements du projet pilote GLACPE développé à Infosol.
L'appropriation du code
m'a permis d'apporter des réécritures du code initial, en particulier de son interface utilisateur.

À un moment donné, il s'est avéré que les projets avaient divergés et qu'il fallait se poser pour faire l'état des lieux
et décider de la suite. Une rencontre a été organisée pour que chacun des projets et des innovations puissent être
confrontés.

La rencontre a été pour moi, très intéressante et très formatrice. Chaque fois qu'un choix était présenté, de nouvelles
idées
émergeaient, renvoyant elles-mêmes vers d'autres idées. La synthèse des deux projets n'était pas une simple
intersection,
ni même juxtaposition de ce que chacun contenait. Le résultat est une solution novatrice, résultat d'une synergie et d'une complémentarité
des idées et des personnes qui communiquent. Cette synergie est parfois représentée sous la forme du 1+1=3 :
deux
éléments peuvent produire quelque chose de plus grand que la somme de leurs parties individuelles. Par la suite, une
solution
a été développée. Les fonctionnalités qui pouvaient être partagées ont été regroupées dans un package (.jar) appelé
noyau de l'application.
Ce noyau contenait aussi des interfaces permettant de développer les fonctionnalités génériques sous la forme de
composition et
d'abstractions présentes dans ce même noyau.

Je finirai cet exemple en relevant combien le temps, celui pour réfléchir à une solution, celui pour réfléchir à ce qui
avait été fait, a pris une part importante dans la simplification du travail ultérieur. Dans l'approche projet, ce temps
est nécessaire pour voir où l'on va, pour ne pas réinventer la roue, pour gagner en efficacité. Même si l'approche
agile [^8]
permet de faire des corrections du projet grâce à un feed-back, elle ne prend généralement pas le projet dans son
ensemble,
et des pauses sont à prévoir pour prendre du recul sur l'ensemble du projet, et son placement dans des objectifs plus
généraux.

L'autre exemple présentera l'organisation de l'assemblée générale. Une assemblée générale d'unité est un exercice
imposé.
Le public est très hétéroclite, tant en termes de compétences, qu'en termes de métiers. Chaque présentation doit être
pensée
à destination de l'ensemble du public.

Quand je suis arrivé dans l'unité, j'ai eu l'impression qu'il s'agissait d'un concours de savoirs. Chaque présentation étant
destinée
à une petite partie de l'auditoire, qui généralement avait déjà eu le droit à la présentation. La grande majorité, se
sentant exclue, vaquait à d'autres occupations. Cependant, certaines présentations semblaient intéresser l'ensemble
de l'auditoire. En particulier les présentations faîtes par les nouveaux arrivants.

Je me suis donc interrogé sur la possibilité de généraliser cela à l'ensemble des sujets et ce que l'on pouvait changer
pour rendre ces Assemblées plus "vivantes".

D'abord questionner le sujet (qu'on le choisisse ou qu'il nous soit imposé). Le sens d'un sujet change selon le public
auquel
il est destiné. Ensuite isoler le-s message-s que l'on veut faire passer.

On peut jouer sur le format : sous forme d'atelier, de quizz, de jeu, de vidéo, de conférence gesticulée... On peut
proposer des Assemblées itinérantes.
J'ai particulièrement apprécié cette forme et me suis donc inscrit à plusieurs reprises pour organiser de telles
assemblées.
On peut utiliser des intervenants ou faire varier les supports. Cette variété peut permettre à chacun de trouver sa
place
à un moment.

Une fois identifié le message et le format, il faut travailler la forme. Je suis partisan de refuser de faire un exposé
si l'on n'a pas le temps de faire ce travail de formalisation. L'effort doit être du côté de l'orateur et non du public.
C'est une forme de respect de son auditoire.

Enfin, il faudra garder son auditoire. Un déroulé clair, des messages simples, des "accroches", le choix du support, le
choix
des examples peuvent y contribuer. Le temps de préparation est primordial pour arriver à cette fin.

Pour traiter des ontologies et du web sémantique, j'ai donc cherché à appliquer ces principes. Sujet très technique,
mais
avec des champs d'application au quotidien ou pour la recherche. J'ai essayé de prendre des exemples concrets dans le
quotidien
ou les points d'intérêt de l'unité. J'ai identifié les éléments importants à la compréhension et cherché comment le
public pouvait se les approprier.

À la suite de cette présentation, les personnes travaillant sur les sols ont pu se saisir de la question d'une ontologie
pour les sols. [^9]

### Tests

J'ai suivi une formation sur les tests. Elle m'a permis de réaliser combien les tests allaient au delà de la seule
vérification du code.
Ils sont aussi une aide au développement, une documentation, une vitrine d'exemple. J'ai donc accordé une place importante
aux tests,
et j'ai fait en sorte que cette pratique soit partagée au sein de l'équipe de développement.

J'ai participé au montage d'une formation d'unité sur les tests avec pour premier objectif que chacun puisse s'approprier
les tests comme un outil et non comme une contrainte.

Suite à la formation, j'ai mis en place, dans le code de l'application, une stratégie de tests fonctionnels.
Le principe est de pouvoir, pour chaque plugin de l'application, tester l'ensemble de l'application avec les fichiers
"exemple" pour ce plugin (dépôt des référentiels, dépôt des fichiers de données) puis de tester une extraction des données
et de le comparer au résultat.

Ces tests permettent :

- de documenter les plugins (le fichier de test correspond au format de dépôt attendu)
- de vérifier la validité du code : avec ces fichiers, on extrait bien ce format de fichier
- de débogger l'application : en mettant des points d'arrêt dans l'exécution du code
- d'assurer la non-régression : lors d'un refactoring par exemple
- d'assurer au développeur que ses corrections permettent toujours de rendre le service attendu

J'ai mis en place dans le noyau de l'application des packages de tests qui permettent de construire facilement un test
fonctionnel
spécifique d'un référentiel ou d'un type de données.

Voici un exemple des packages de l'application f-ore-t importés depuis le noyau.
L'application est découpée en modules de fonctionnalités (par exemple metadata pour les référentiels). Pour chaque module,
on aura une implémentation dans le plugin. On pourra tester cette implémentation en utilisant les outils de tests du
package de
tests associé (:test-jar:).

- Maven: org.inra.ecoinfo:kernel-config-loader:1912.07-SNAPSHOT
- Maven: org.inra.ecoinfo:kernel-core-services-api:1912.07-SNAPSHOT
- Maven: org.inra.ecoinfo:kernel-core-services-impl:1912.07-SNAPSHOT
- Maven: org.inra.ecoinfo:kernel-core-services-impl:test-jar:tests:1912.07-SNAPSHOT
- Maven: org.inra.ecoinfo:kernel-core-ui-jsf:1912.07-SNAPSHOT
- Maven: org.inra.ecoinfo:kernel-dataset-services-api:1912.07-SNAPSHOT
- Maven: org.inra.ecoinfo:kernel-dataset-services-impl:1912.07-SNAPSHOT
- Maven: org.inra.ecoinfo:kernel-dataset-services-impl:test-jar:tests:1912.07-SNAPSHOT
- Maven: org.inra.ecoinfo:kernel-dataset-ui-jsf:1912.07-SNAPSHOT
- Maven: org.inra.ecoinfo:kernel-deserialization-loader:1912.07-SNAPSHOT
- Maven: org.inra.ecoinfo:kernel-extraction-services-api:1912.07-SNAPSHOT
- Maven: org.inra.ecoinfo:kernel-extraction-services-impl:1912.07-SNAPSHOT
- Maven: org.inra.ecoinfo:kernel-extraction-services-impl:test-jar:tests:1912.07-SNAPSHOT
- Maven: org.inra.ecoinfo:kernel-extraction-ui-jsf:1912.07-SNAPSHOT
- Maven: org.inra.ecoinfo:kernel-filecomp-services-api:1912.07-SNAPSHOT
- Maven: org.inra.ecoinfo:kernel-filecomp-services-impl:1912.07-SNAPSHOT
- Maven: org.inra.ecoinfo:kernel-filecomp-services-impl:test-jar:tests:1912.07-SNAPSHOT
- Maven: org.inra.ecoinfo:kernel-filecomp-ui-jsf:1912.07-SNAPSHOT
- Maven: org.inra.ecoinfo:kernel-metadata-services-api:1912.07-SNAPSHOT
- <span style="background-color: yellow"> Maven: org.inra.ecoinfo:kernel-metadata-services-impl:1912.07-SNAPSHOT</span>
- <span style="background-color: yellow">Maven: org.inra.ecoinfo:kernel-metadata-services-impl:test-jar:tests:
  1912.07-SNAPSHOT</span>
- Maven: org.inra.ecoinfo:kernel-metadata-ui-jsf:1912.07-SNAPSHOT
- Maven: org.inra.ecoinfo:kernel-mga-api:1912.07-SNAPSHOT
- Maven: org.inra.ecoinfo:kernel-mga-impl:1912.07-SNAPSHOT
- Maven: org.inra.ecoinfo:kernel-mga-ui-jsf:1912.07-SNAPSHOT
- Maven: org.inra.ecoinfo:kernel-mga-ui-jsf:test-jar:tests:1912.07-SNAPSHOT
- Maven: org.inra.ecoinfo:kernel-serialization-loader:1912.07-SNAPSHOT
- Maven: org.inra.ecoinfo:kernel-synthesis-services-api:1912.07-SNAPSHOT
- Maven: org.inra.ecoinfo:kernel-synthesis-services-impl:1912.07-SNAPSHOT
- Maven: org.inra.ecoinfo:kernel-synthesis-ui-jsf:1912.07-SNAPSHOT
- Maven: org.inra.ecoinfo:kernel-system-services-api:1912.07-SNAPSHOT
- Maven: org.inra.ecoinfo:kernel-system-services-impl:1912.07-SNAPSHOT
- Maven: org.inra.ecoinfo:kernel-system-ui-jsf:1912.07-SNAPSHOT
- Maven: org.inra.ecoinfo:kernel-utils:1912.07-SNAPSHOT

Grâce à cette stratégie, les développeurs d'un plugin peuvent tester leurs développements, et, l'appropriation de ces tests
par un tiers est facilitée, du fait qu'ils s'appuient sur des outils communs. [^10]

### Détournement de code (hacking)

Il y a deux raisons à vouloir détourner du code. La première parce que l'on veut utiliser un code qui ne répond que
partiellement à notre demande. La deuxième quand l'on utilise un code et que son détournement permettrait de le
rendre plus efficace. Je vais illustrer les deux exemples.

#### Détournement d'un logiciel

#### Détournement d'une fonctionnalité

Dans l'application v1510 des ORE, il s'est avéré à un moment que la stratégie d'enregistrement des droits était lente et
qu'il y avait des effets de bord. La cellule a réfléchi à une nouvelle stratégie. Elle a été développée par RACHID
YAHIAOUI.
J'ai eu en charge l'intégration de ce module de droits dans l'application, vers une nouvelle version du noyau v1912.  
Lors de l’intégration de cette fonctionnalité, je me
suis rendu compte qu’il était possible de l’utiliser pour résoudre d’autres demandes en attente
sur la forge. C’est ainsi que la gestion des droits en utilsant des groupes de droits, pouvait être
faite avec peu de code. Une fois l’implémentation des groupes faite, il fallait réfléchir aux
règles et aux interfaces nécessaires à son utilisation.
Après plusieurs échanges avec les membres de l’équipe, j'ai produit un document décrivant
cette fonctionnalité et faisant apparaître les modifications au niveau de l’interface. [^11].

Cette même arborescence de droits pouvait, juste en la "regardant" autrement, permettre d'associer un fichier à un objet
du domaine, là où l'on associait un groupe à un objet du domaine. En utilisant un jeu d'interface, on pouvait prendre
un utilisateur
ou un fichier pour réaliser cette association.

### Réécriture de code (refactoring)

Le refactoring est souvent une activité du quotidien : changer la signature d'une méthode, découper du code, abstraire une logique, rendre
générique une classe, regrouper du code semblable... Dans les méthodes agiles, il est fréquemment de faible portée, lié
à la partie
du code en cours de travail. Mais parfois après plusieurs "runs", il faut prendre le temps de se poser, et de voir sur
l'ensemble du code si
ce travail de réécriture peut être effectué.

Je ne re-décris pas les exemples précédents qui sont des exemples de refactoring. Voici d'autres exemples qui relèvent d'une réflexion.

#### Fusion des interfaces de dépôt, validation, publication.

Dans la version pilote, pour publier des données (les sauvegarder en base), il y avait trois panneaux semblables.

- un panneau de dépôt de fichier sur un nœud de l'arbre des objets du domaine.
- un panneau de validation du fichier. Dans ce panneau, il était possible de soumettre plusieurs versions du même
  fichier.
- un panneau de publication afin de mettre à jour la base de données.

Les trois panneaux étaient semblables. Il m'a semblé judicieux de les fusionner dans un seul panneau. Pour gérer les
droits et la temporalité,
je propose des boutons contextuels (visibles ou non ; actifs ou non ).

Dans le même temps, je repense la manière dont les fichiers sont enregistrés dans la base de données. Je propose de
créer
deux classes représentant des objets virtuels.

- une classe jeu de données (Dataset) qui décrit le contexte des données d'un fichier (spatial et temporel), et
  représentant
  l'ensemble des versions d'un même fichier.
- une classe version qui décrit une version d'un fichier.

La classe Dataset peut contenir plusieurs versions d'un même fichier, mais un seul de ces fichiers peut être publié.

Parallèlement, je mets en place une mécanique d'enregistrement des fichiers :

- au dépôt du fichier un jeu de données est créé et le fichier est ajouté à la liste des versions avec le numéro de
  version 1.
- au dépôt d'un fichier, si la version existe déjà, le fichier est simplement ajouté à la liste des versions avec un
  numéro de version incrémenté.
- lorsque le fichier est publié, on renseigne son identifiant dans le champ publishedVersion du dataset.
- lorsque le fichier est dépublié, on supprime son identifiant du champ publishedVersion du dataset.
- lorsqu'un fichier est supprimé, on le dépublie d'abord si nécessaire, puis on le supprime.
  Si le jeu de données ne contient plus de version, il est supprimé.

On voit dans cet exemple que le travail de réécriture de l'interface entraîne de profond changements aussi dans le code
et
la création de nouveaux concepts pour la description du métier. La créativité prend une part importante dans ce
développement.
L'idée de départ (fusion d'interface) entraîne une réécriture du code profonde. Elle fait apparaître de nouveaux
concepts et
un vocabulaire est créé pour désigner des objets métiers.

### Reinvention de code (changement de paradigme)

La genèse d'OPENAdom est un bel exemple de créativité. L'histoire débute avec un audit de l'application. Celle-ci arrive
en
fin de course : technologies vieillissantes, portage de version compliqué, pile logicielle complexe et difficilement
abordable
par des stagiaires, lenteurs...

La cellule externalise auprès de "code Lutin" la réalisation d'un audit et la livraison d'un POC (Proof Of Concept)
d'une nouvelle solution.

Je me souviens de ma réaction en deux temps lors de la présentation de cette solution. D'abord une inquietude en
comprenant
que tous les développements de l'application allaient être obsolètes, et que seule l'analyse subsisterait. Ensuite est
venue
une période d'appropriation. La solution proposée est radicalement différente de la solution initiale. Il faut
comprendre
ce qu'elle propose, ensuite imaginer comment les utilisateurs pourront retrouver les fonctionnalités. Est-ce que la
solution
rendra les mêmes services ? Est-ce que tous les cas recensés dans les différents ORE pourront être traités par
OPENAdom?...

Dans l'application historique, pour chaque format de fichiers déposés dans l'application, un ensemble de code est
développé.
Le temps de développement pour un type de fichier est conséquent, il demande une connaissance approfondie de
l'application.
Le développeur doit maîtriser l'ensemble de la pile technologique (base de données, métier, interface).

OPENAdom simplifie radicalement cette approche. L'utilisateur décrit son domaine au format texte (yaml) dans un fichier
de configuration. Il suffit ensuite de déposer ce fichier dans l'application et cette dernière met en place un SI dédié.
L'utilisateur peut alors déposer ses fichiers de données et extraire ses données.

C'est donc le travail d'analyse qui devient prépondérant dans la mise en œuvre d'un SI. Les développements deviennent
marginaux.
Par la suite, il est possible de développer des applications s'appuyant sur la base de données, directement ou via les
web services.
Les compétences des développeurs s'attelant à cette tâche n'étant pas contrainte. Tous les outils et toutes les
technologies
pouvant être utilisés.

De plus la base de données créée à un schéma unique. Un outil créé pour un SI peut facilement être réutilisé pour un
autre.
J'ai pu par exemple montrer que dans l'outil de data visualisation "Grafana", 4 requêtes permettaient de générer
l'ensemble
des graphes de répartition des données.[^12]

Je ne vais pas faire ici une liste exhaustive de mes contributions à OPENAdom. Elles sont visibles dans la forge dédiée [^13}]
On y trouve en particulier de nombreuses réflexions pour faire évoluer l'application ou pour relever des risques.

## Conclusion

J'ai conscience d'avoir rédigé mon rapport de carrière sous un angle peu commun. Je forme le souhait d'avoir suscité 
votre intérêt en choisissant une thématique de réflexion pour présenter mes fonctions de développeur dans la cellule Eco-Informatique ORE.

J'ai pu, au travers de cette approche, présenter une analyse personnelle et critique de la dynamique de cete activité, 
ainsi que les différents outils que j'ai pu utiliser.

Je pense avoir montré comment la créativité prenait une part prépondérante dans le développement informatique. Dans une approche
agile, elle permet de replacer un "run" dans le contexte plus large de l'ensemble de l'application, de sa place au sein d'autres 
projets, de son évolution passée et à venir. 

[^1]: Connaissance des langages (php, java, javascript, sql...), des outils (IDE - Interface de développement-,
frameworks,
applications... -), des méthodes (Méthodes agiles, TDD - Test Driven Development, Onion architecture ... - ), des
pratiques (
bonnes pratiques, conventions, Design patterns, API - application programme interface- , les standards...)
)

[^2]: Un programme informatique est avant tout un objet mathématique. Il peut être considéré comme une fonction prenant
des paramètres en entrée et un résultat en sortie. La logique est donc une compétence nécessaire au développement
informatique.
La résolution de problèmes est aussi au cœur du métier. Tant pour comprendre la demande que pour résoudre des anomalies.

[^3]: L'analyse permet de transformer un problème du monde réel en une solution informatique. Il faut parfois comprendre
le besoin en le questionnant au-delà de sa formulation. Par exemple, j'ai besoin d'un bouton pour ... au lieu de,
j'aimerais
pouvoir visualiser... Les deux formulations ne sont pas équivalentes. Une écoute empathique du besoin est nécessaire
pour
fournir une solution efficace à une problématique donnée. Certaines formulations, si elles ne sont pas suffisamment
questionnées,
seront source d'erreurs. Plus les problématiques sont détectées précocement, moins il est coûteux d'y apporter des
solutions.

[^4]: Les outils sont aussi bien des applications, des frameworks, des langages que des supports (IDE, outils de
versionnement,
outils de déploiement). Mais au-delà des outils, les nombreuses réflexions sur les méthodologies viennent bouleverser la
façon
de programmer, allant même jusqu'à transformer ou créer de nouveaux métiers.

[^5]: 2^15 équivaut à 32 768 mots. Soit une complexité de O(log(n))

[^6]: L'ontologie développée dans le cadre d'ANNEE-France, basée sur l'ontologie OBOE, permet, grâce au travail
d'Infosol
entre autre, de créer une base sémantique, en s'appuyant sur la base de données du Si des SOERE. On peut ainsi proposer
des outils génériques (édition de jeux de données de DOI ou de métadonnées...).
Depuis ma présentation, les données sol cherchent aussi à se doter d'une ontologie. OpenADOM est depuis proposé dans de
nombreux
projet comme solution de création de SI.

[^7]: https://ptcherniati.pages.mia.inra.fr/cei/Documents/nommage_variables.odt

[^8]: L'approche agile est une méthode de gestion de projet qui se concentre sur la flexibilité,
l'adaptabilité et la collaboration. Les principes fondamentaux de l'approche agile sont énoncés dans le "Manifeste
Agile",
qui prône notamment la priorité donnée à la satisfaction du client, la livraison rapide et fréquente de versions
fonctionnelles, et la collaboration étroite entre les membres de l'équipe.

[^9]: https://ptcherniati.pages.mia.inra.fr/cei/Documents/ontologies.pptx

[^10] Je joins l'ensemble des classes de tests fonctionnels pour les données de flux gazeux (cf.
Documents/classes_de_test_fluxgazeux).
On voit que chacune de ces classes hérite de classe de tests du noyau.

[^11] Un diagramme de classe des droits dans
v1709 https://ptcherniati.pages.mia.inra.fr/cei/Documents/droits/groupes.pdf

[^12]
Voici la requête permettant de générer un graphe (date/ valeur par aggregation). Cette requête
lit les informations de synthèse du fichier de configuration pour savoir comment afficher une courbe
depuis les données de la table "data". Quelque soit l'application, le type de données, si cette description
est présente alors un graphique peut être affiché. 
```̀sql 
with variable as (select '$variable' variable, '$datatype' datatypename),
synthesis as (
select variable,
configuration #>> Array['datatypes',datatypename, 'data',variable,'chartdescription','value'] "value",
configuration #>>  Array['datatypes',datatypename, 'data',variable,'chartdescription','aggregation', 'variable'] aggregationVariable,
configuration #>>  Array['datatypes',datatypename, 'data',variable,'chartdescription','aggregation', 'component'] aggregationComponent,
configuration #>>  Array['datatypes',datatypename, 'authorization','timescope','variable'] timescopeVariable,
configuration #>>  Array['datatypes',datatypename, 'authorization','timescope','component'] timescopeComponent
from application, variable
where name='$application' ),
datavalues as (
select jsonb_object_agg(datavalues) "values"
from $application.data , variable where datatype = datatypename
group by rowid)
select (("values" #>> Array[timescopeVariable, timescopeComponent])::composite_date)::timestamp as "time",
Coalesce(("values" #>> Array[aggregationVariable,aggregationComponent]), variable) as metric,
replace(("values" #>> Array[variable,value]), ',','.')::numeric
from synthesis, datavalues
where replace(("values" #>> Array[variable,value]), ',','.')::numeric != -9999
order by 1,2
```

[^13] https://forgemia.inra.fr/anaee-dev/si-ore-v2

[^14] https://forgemia.inra.fr/anaee-dev/si-ore-v2/-/boards/2050