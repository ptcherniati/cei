``` mermaid
classDiagram
direction BT
class composite_node_data_set {
   composite_node_data_set: integer index
   composite_node_data_set: bigint id_ancestor
   composite_node_data_set:  bigint id_parent_node
   composite_node_data_set: bigint realnode
   composite_node_data_set: bigint branch_node_id
}
class composite_nodeable {
   varchar(31) dtype
   varchar(255) code
   bigint ordernumber
   bigint id
}
class filetype {
   varchar(255) code
   text description
   varchar(255) name
   bigint ft_id
}
class group_filecomp {
   bigint group_id
   bigint file_comp_id
}
class group_utilisateur {
   bigint usr_id
   bigint group_id
}
class groupe {
   text activityasstring
   activityasstring : varchar(255) group_name
   activityasstring : varchar(255) group_which_tree
   activityasstring :  varchar(255) group_type
   activityasstring : bigint id
}
class insertion_filecomp_ifcs {
   varchar(255) ifcs_code
   varchar(255) contenttype
   timestamp ifcs_create_date
   oid data
   date ifcs_date_debut_periode
   date ifcs_date_fin_periode
   text ifsc_description
   varchar(255) ifcs_name
   boolean forapplication
   timestamp ifcs_last_modify_date
   boolean mandatory
   integer ivfc_size
   bigint ifcs_create_user
   bigint ft_id
   bigint ifcs_last_modify_user
   bigint ifcs_id
}
class realnode {
   varchar(255) path
   bigint id_ancestor
   bigint id_nodeable
   bigint id_parent_node
   bigint id
}
class utilisateur {
   varchar(255) language
   boolean active
   varchar(255) email
   varchar(255) emploi
   boolean isroot
   varchar(255) login
   varchar(255) nom
   varchar(255) password
   varchar(255) poste
   varchar(255) prenom
   bigint id
}

composite_node_data_set  "branch_node_id"--> "id_ancestor"  composite_node_data_set 
composite_node_data_set  "branch_node_id"--> "id_parent_node"  composite_node_data_set
composite_node_data_set  "id"--> "realnode"  realnode 
group_filecomp  "id"--> "group_id"   groupe 
group_filecomp  "ifcs_id"--> "file_comp_id"   insertion_filecomp_ifcs
group_utilisateur  "id"--> "groupe_id"  groupe 
group_utilisateur  "id"-->  "usr_id" utilisateur 
insertion_filecomp_ifcs  "ft_id"-->  "filetype" filetype 
insertion_filecomp_ifcs  "id"-->  "ifcs_last_modify_user" utilisateur 
insertion_filecomp_ifcs  "id"-->  "ifcs_create_user" utilisateur 
realnode  "id"-->  "id_nodeable" composite_nodeable 
realnode  "id"-->  "id_parent_node" realnode 
realnode  "id"-->  "id_ancestor" realnode 

```